package top.cavecraft.purgatoryotf;

import net.md_5.bungee.api.plugin.Plugin;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static top.cavecraft.purgatoryotf.OTFServer.allOTFServers;

public final class PurgatoryOTF extends Plugin {
    AsynchronousServerSocketChannel server;
    Future<AsynchronousSocketChannel> socket;

    @Override
    public void onEnable() {
        try {
            server = AsynchronousServerSocketChannel.open();
            server.bind(new InetSocketAddress("0.0.0.0", 25564)); //Block this port with a firewall! It is bound to 0.0.0.0 so you can connect external servers.
            socket = server.accept();
            getProxy().getScheduler().schedule(this, () -> {
                //Accept new servers
                if (socket.isDone()) {
                    try {
                        AsynchronousSocketChannel socketChannel = socket.get();
                        String ip = ((InetSocketAddress) socketChannel.getLocalAddress()).getHostName();
                        ByteBuffer portBuffer = ByteBuffer.allocate(64);
                        socketChannel.read(portBuffer).get(); //This is synchronous and bad but it should never block if the downstream server has the ConnectToPurgatory plugin.
                        InetSocketAddress address = new InetSocketAddress(ip, portBuffer.getInt());
                        new OTFServer(address);
                        socket = server.accept();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //Collect dead servers
                ArrayList<OTFServer> otfServersToRemove = new ArrayList<>();
                for (OTFServer otfServer : allOTFServers) {
                    if (otfServer.checkIfShutdown()) {
                        otfServersToRemove.add(otfServer);
                    }
                }
                for (OTFServer otfServer : otfServersToRemove) {
                    allOTFServers.remove(otfServer);
                }
            }, 0, 1, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
