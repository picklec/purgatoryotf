package top.cavecraft.purgatoryotf.util;

import net.md_5.bungee.api.config.ServerInfo;


import java.net.InetSocketAddress;

import static top.cavecraft.purgatoryotf.util.BungeeUtils.getProxy;

public class ServerUtils {
    public static ServerInfo addServer(InetSocketAddress address) {
        String name = "otfs" + Math.round(Math.random()*100) + System.nanoTime();
        ServerInfo serverInfo = getProxy().constructServerInfo(name, address, "motd", false);
        getProxy().getServers().put(name, serverInfo);
        return serverInfo;
    }

    public static void removeServer(int port) {
        ServerInfo toRemove = null;
        for (ServerInfo serverInfo : getProxy().getServers().values()) {
            if (((InetSocketAddress)serverInfo.getSocketAddress()).getPort() == port) {
                toRemove = serverInfo;
            }
        }
        if (toRemove != null) {
            getProxy().getServers().remove(toRemove.getName());
        }
    }
}
